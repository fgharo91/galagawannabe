package Models;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.ImageIcon;

public class Sprite {

	protected ImageIcon img;
	protected Point location;
	protected int moveLeft, moveRight, moveUp, moveDown;

	public Sprite(ImageIcon img, Point location) {
		this.setImg(img);
		this.setLocation(location);
	}
	
	public Sprite(ImageIcon img, Point location, int moveLeft, int moveRight, int moveUp, int moveDown) {
		this.setImg(img);
		this.setLocation(location);
		this.setMoveLeft(moveLeft);
		this.setMoveRight(moveRight);
		this.setMoveUp(moveUp);
		this.setMoveDown(moveDown);
	}

	public void draw(Component comp, Graphics page){
		img.paintIcon(comp, page, location.x, location.y);			
	}
	
	public ImageIcon getImg() {
		return img;
	}

	public void setImg(ImageIcon img) {
		this.img = img;
	}

	public int getIconWidth() {
		return img.getIconWidth();
	}

	public int getIconHeight() {
		return img.getIconHeight();
	}

	public Point getLocation() {
		return location;
	}

	public void setLocation(Point location) {
		this.location = location;
	}
	
	public boolean collided(Sprite sprite){
		// Case 1. This Sprite has a point on its boundary 
		// that is equal to some point on sprite's boundary.
		// For each point on this sprite compare it to all 
		// the points on the incoming sprite. As soon as one such 
		// point is found return true.
		// Checking this y's top horizontal boundary line.
		if((sprite.location.y <= this.location.y && this.location.y <= sprite.location.y + sprite.getIconHeight()) && 
				((this.location.x <= sprite.location.x && sprite.location.x <= this.location.x + this.getIconWidth()) || 
				 (this.location.x <= sprite.location.x + sprite.getIconWidth() && sprite.location.x + sprite.getIconWidth() <= this.location.x + this.getIconWidth()))) return true;
		// Checking this y's lower horizontal boundary line. TODO
		if((sprite.location.y <= this.location.y + this.getIconHeight() && this.location.y + this.getIconHeight() <= sprite.location.y + sprite.getIconHeight()) && 
				((this.location.x <= sprite.location.x && sprite.location.x <= this.location.x + this.getIconWidth()) || 
				 (this.location.x <= sprite.location.x + sprite.getIconWidth() && sprite.location.x + sprite.getIconWidth() <= this.location.x + this.getIconWidth()))) return true;
		
		return false;
	}

	public void setMoveLeft(int moveLeft) {
		this.moveLeft = moveLeft;
	}
	
	public void setMoveRight(int moveRight) {
		this.moveRight = moveRight;
	}
	
	public void setMoveUp(int moveUp) {
		this.moveUp = moveUp;
	}
	
	public void setMoveDown(int moveDown) {
		this.moveDown = moveDown;
	}
	
	public void moveLeft() {
		location = new Point(location.x - moveLeft, location.y);
	}
	
	public void moveRight() {
		location = new Point(location.x + moveRight, location.y);
	}
	
	public void moveUp() {
		location = new Point(location.x, location.y - moveUp);
	}
	
	public void moveDown() {
		location = new Point(location.x, location.y + moveDown);
	}

}
