package Models;

import java.awt.Point;
import java.util.ArrayList;

import javax.swing.ImageIcon;

public class ShooterSprite extends Sprite {
	private final int FIRE_BALL_STEP = 5;
	private final int FIRE_BALL_STEP_DELAY = 40;
		
	//private ArrayList<Sprite> fire; 
	// This is risky and might not work. Figure out how to fix this later
	// with locks. Since fire will be accessed by the keylistener thread to add a new fireBall, accessed
	// by shotTimer listener to update the movement of the shots, and by the screen paint method to paint.
	// What happens if paint trys to get a fire from the gun while the listeners either delete them or 
	// add to them? Race condition.
	private ImageIcon fireImg;
	private int score;
	
	public ShooterSprite(ImageIcon shooterImg, ImageIcon fireImg, Point location) {
		super(shooterImg, location);
		this.fireImg = fireImg;
		score = 0;
	}
	
	public ShooterSprite(ImageIcon img, ImageIcon fireImg, Point location, int moveLeft, int moveRight, int moveUp, int moveDown) {
		super(img, location, moveLeft, moveRight, moveUp, moveDown);
		this.fireImg = fireImg;
		score = 0;
	}
	
	public Sprite shoot() {
		if(fireImg == null) System.out.println("hello world");
		return new 
		Sprite
		(fireImg, 
		 new Point( (location.x + this.getIconWidth() / 2) - (this.fireImg.getIconWidth() / 2),	location.y - fireImg.getIconHeight()));	
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

//	public ArrayList<Sprite> getGun() {
//		
//	}
	
	public final int getFireStep() {
		return FIRE_BALL_STEP;
	}
	
	public final int getFireStepDelay() {
		return FIRE_BALL_STEP_DELAY;
	}
	

	
	
	
	

}
