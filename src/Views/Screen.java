package Views;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;

import Models.ShooterSprite;
import Models.Sprite;

public class Screen extends Canvas {
	// Dimensions of screen and its background color.
	public final static int 
	SCREEN_WIDTH=500, SCREEN_HEIGHT=500;
	public final static Color
	screenColor = Color.black;
	public int drawingState = 0; // 0 is for start, 1 for play, and 2 for end.
	
	// Labels of screen to paint.
	public final static String 
	startTitLabel = "Shooting Aliens", 
	startSubTitLabel = "(click on screen to start game)", 
	endLabel = "Game Over!";
	
	public String 
	secsLeftLabel, 
	playerScoreLabel;
	
	// Points at where to draw labels.
	public final static Point 
	startTitLabelPt = new Point(SCREEN_WIDTH / 2 - 75, SCREEN_HEIGHT / 2 - 30), 
	startSubTitLabelPt = new Point(SCREEN_WIDTH / 2 - 60, SCREEN_HEIGHT / 2 - 10),
	endLabelPt = new Point(SCREEN_WIDTH / 2 - 50, SCREEN_HEIGHT / 2 - 30),
	secsLeftLabelPt = new Point(5, 15),
	playerScoreLabelPt = new Point(100, 15);
	
	// Models to paint. (Consider making these references only to the ImageIcons)?
	public ShooterSprite shooter;
	public ArrayList<Sprite> fireBalls, aliens;
	
	// GAME TECHNIQUE 1: Page flipping & Accelerated Graphics
	// A strategy object for managing swapping of buffers.
	// This supports Page flipping & Accelerated Graphics.
	public BufferStrategy strategy;
	
	public Screen(ShooterSprite shooter, ArrayList<Sprite> fireBalls, ArrayList<Sprite> aliens, int timeLeft, int playerScore) {
		this.setPreferredSize(new Dimension(SCREEN_WIDTH, SCREEN_HEIGHT));
		this.shooter = shooter; 
		this.fireBalls = fireBalls; 
		this.aliens = aliens;
		setSecsLeftLabel(timeLeft); 
		setScoreLabel(playerScore);
		
		// GAME TECHNIQUE 1: Accelerated Graphics
		// Tell AWT not to bother repainting our canvas since we're
		// going to do that our self in accelerated mode
		this.setIgnoreRepaint(true);
	}
	
	public void setBufferStrategy() {		
		// create the buffering strategy which will allow AWT
		// to manage our accelerated graphics
		this.createBufferStrategy(2);
		strategy = getBufferStrategy();
	}
	
	public void renderGraphics() {
		// Get hold of a graphics context for the accelerated 
		// surface and blank it out
		java.awt.Graphics2D g = (java.awt.Graphics2D) strategy.getDrawGraphics();
		
		super.paint(g);
		this.setBackground(screenColor);
		
		switch(drawingState){
			case 0:
				paintTitLabel(Color.green, g);
				paintSubTitLabel(Color.white, g);				
				paintStatLabels(Color.white, g);
				// paint collisions if any.
				try{
					paintShooter(g);
				}catch(Exception e) {
					
				}
				break;
			case 1:
				try{
					paintFireBalls(g);	
					// Watch out for java.util.ConcurrentModificationException
					// instead of trying and catching it, think of a better design
					// mechanism to avoid the exception all together.
					paintAliens(g);
					// paint collisions if any.
					paintShooter(g);
				}catch(Exception e) {
					
				}
				paintStatLabels(Color.white, g);
				break;	
			case 2:				
				this.setBackground(Color.black);
				paintStatLabels(Color.white, g);
				paintEndLabels(Color.green, g);
				try{
					paintShooter(g);
				}catch(Exception e) {
					
				}
				break;
		}		

		// finally, we've completed drawing so clear up the graphics
		// and flip the buffer over
		g.dispose();
		strategy.show();
	}
	
	// set methods for updating the view Screen.
	public void setSecsLeftLabel(int secsLeft) {
		secsLeftLabel = "Time left: " + secsLeft;
	}
	
	public void setScoreLabel(int playerScore) {
		playerScoreLabel = "Aliens killed: " + playerScore;
	}
	
	public void setDrawingState(int drawingState) {
		this.drawingState = drawingState;
	}
	
	// Paint methods for drawing the graphics. NOt using AWT to paint graphics and instead using 
	// 
	@Override
	public void paint(Graphics page){
		
	}
	
	public void paintTitLabel(Color color, Graphics page) {
		page.setColor(color);
		page.drawString(startTitLabel, startTitLabelPt.x, startTitLabelPt.y);
	}
	
	public void paintSubTitLabel(Color color, Graphics page) {
		page.setColor(color);
		page.drawString(startSubTitLabel, startSubTitLabelPt.x, startSubTitLabelPt.y);
		
	}
	
	public void paintStatLabels(Color color, Graphics page) {
		page.setColor(color);
		page.drawString(secsLeftLabel + " s", secsLeftLabelPt.x, secsLeftLabelPt.y);
		page.drawString(playerScoreLabel, playerScoreLabelPt.x, playerScoreLabelPt.y);		
	}
	
	public void paintEndLabels(Color color, Graphics page) {
		page.setColor(color);
		page.drawString(endLabel, endLabelPt.x, endLabelPt.y);
	}
	
	public void paintShooter(Graphics page) throws java.util.ConcurrentModificationException{
		shooter.draw(this, page);
	}
	
	public void paintFireBalls(Graphics page) throws java.util.ConcurrentModificationException{
		
		for(Sprite fire:fireBalls)
			fire.draw(this, page);
	}
	
	public void paintAliens(Graphics page) throws java.util.ConcurrentModificationException{
		for(Sprite alien: aliens)
			alien.draw(this, page);
	}

}
