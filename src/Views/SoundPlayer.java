package Views;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.Line;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import Resources.Resources;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

public class SoundPlayer {

	// Reference that will have the file names.
	Resources res;

	// Declare an AudioStream object from will hold the file input stream.
	private AudioStream startas, shootas, alienexpas;     

	
	public SoundPlayer(Resources res){
		this.res = res;		   
	}
	
	public void playGameStartSound(){
		loadStartSound();
		if(startas != null)
			AudioPlayer.player.start(startas);
	}
	
	public void playShootingSound(){
		loadShootSound();
		if(shootas != null) 
			AudioPlayer.player.start(shootas); 
	}
	
	public void playEnemyExpSound(){
		loadEnemyExpSound();
		if(alienexpas != null)
			AudioPlayer.player.start(alienexpas);           
	}
	
	public void loadStartSound() {
		startas = res.getStartas();
	}
	
	public void loadShootSound(){ 
		shootas = res.getShootas();
	}
	
	public void loadEnemyExpSound(){
		alienexpas = res.getAlienExpas();
	}


}
