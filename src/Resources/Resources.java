package Resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.ImageIcon;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.Line;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import sun.audio.AudioStream;

public class Resources {
	
	// Music file names (.wav) "E:\\Programming\\Java\\2014SummerProjects\\ShootingAliens\\src\\galagaStart.wav"
	public static final String gameStartSoundFileName = new String("/galagaStart.wav");
	public static final String shootSoundFileName = new String("/galagaFire.wav");
	public static final String alienExpSoundFileName = new String("/galagaExplosion.wav");
	
	// What is the difference between declaring a field as
	// gameStartSoundFileName = "blblbl"
	// and gameStartSoundFileName = new String("asdf");
	// Picture file namess (.png)
	public static final String shooterImgFileName = new String("/shooter.png");
	public static final String fireImgFileName = new String("/ammoShots.png");
	public static final String alienImg1FileName = new String("/alien1.png");
	public static final String alienImg2FileName = new String("/alien2.png");
	public static final String alienImg3FileName = new String("/alien3.png");
	
	// Images for sprites.
	private ImageIcon playerImg = null;
	private ImageIcon fireBallImg = null;
	private ImageIcon alienImg1 = null;
	private ImageIcon alienImg2 = null;
	private ImageIcon alienImg3 = null;
	
	public Resources(){
			// Loading the image resources.
			playerImg = new ImageIcon(this.getClass().getResource(shooterImgFileName));
			fireBallImg = new ImageIcon(this.getClass().getResource(fireImgFileName));
			alienImg1 = new ImageIcon(this.getClass().getResource(alienImg1FileName));
			alienImg2 = new ImageIcon(this.getClass().getResource(alienImg2FileName));
			alienImg3 = new ImageIcon(this.getClass().getResource(alienImg3FileName));
	}
	
	public ImageIcon getPlayerImg() {
		return playerImg;
	}
	
	public ImageIcon getFireBallImg() {
		return fireBallImg;
	}
	
	public ImageIcon getAlienImg1() {
		return alienImg1;
	}
	
	public ImageIcon getAlienImg2() {
		return alienImg2;
	}	
	
	public ImageIcon getAlienImg3() {
		return alienImg3;
	}
	
	public AudioStream getStartas() {		
		try {
			//InputStream is = 
			//new FileInputStream(Resources.gameStartSoundFileName);
			return new AudioStream(this.getClass().getResourceAsStream(Resources.gameStartSoundFileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		};
		return null;
	}

	public AudioStream getShootas() {		
		try {
			//InputStream is = 
			//new FileInputStream(Resources.shootSoundFileName);
			return new AudioStream(this.getClass().getResourceAsStream(Resources.shootSoundFileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return null;
	}

	public AudioStream getAlienExpas() {
		try {
			//InputStream is = new FileInputStream(Resources.alienExpSoundFileName);
			return new AudioStream(this.getClass().getResourceAsStream(Resources.alienExpSoundFileName));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		return null;
	}

	
}
