package Controls;

import java.awt.Canvas;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.Timer;

import Models.ShooterSprite;
import Models.Sprite;
import Resources.Resources;
import Views.Screen;
import Views.SoundPlayer;

public class GameEngine {
	// Game models
	public ShooterSprite player;
	public ArrayList<Sprite> shots, aliens;
	public static int gameSecondsLeft = 60;
	
	// Game views. 
	public Screen view;
	public SoundPlayer splayer;
	public Resources res;
	
	
	// Game threads to control different entities of the game.
	public Timer gameTimer, playerShotsTimer, alienSpawnTimer, aliensMoveTimer;
	public KeyBoardListener keys;
	public GameClickListener mouse;
	
	// The game states are menu, play, end.
	public int gameState = 0; // 0 is for menu, 1 for play, and 2 for end.	
	public int aType = 0; // 0 - alien1, 1 - alien2, 2 - alien3
	
	public GameEngine(ShooterSprite player, ArrayList<Sprite> shots, ArrayList<Sprite> aliens, Screen view, SoundPlayer splayer, Resources res) {
		// linking game models and views.
		this.player = player;
		this.shots = shots;
		this.aliens = aliens;
		this.view = view;
		this.splayer = splayer;
		this.res = res;
				
		// creating the necessary threads.
		GameEntityDelayListener entityListener = new GameEntityDelayListener();
		gameTimer = new Timer(GameEntityDelayListener.GAME_TIME_DELAY, entityListener);
		playerShotsTimer = new Timer(GameEntityDelayListener.PLAYER_SHOTS_DELAY,entityListener);
		alienSpawnTimer = new Timer(GameEntityDelayListener.ALIEN_SPAWN_DELAY,entityListener);
		aliensMoveTimer = new Timer(GameEntityDelayListener.ALIENS_MOVE_DELAY, entityListener);
		mouse = new GameClickListener();
		keys = new KeyBoardListener();
		
		this.view.addKeyListener(keys);
		this.view.setFocusable(true);
	}

	// Main loop of the game.
	public void play() {
		// create the buffering strategy which will allow AWT
		// to manage our accelerated graphics
		view.setBufferStrategy();

		// Stall the game until the user has clicked to start.
		// When they click it will start the timers.
		view.addMouseListener(mouse);

		// All this loop does so far is keep rendering the drawing until gameSecondsLeft <=0 :/
		long lastLoopTime = System.currentTimeMillis();
		do{
	
			// work out how long its been since the last update, this
			// will be used to calculate how far the entities should
			// move this loop. DON'T NEED THIS SINCE ALL THE OTHER ENTITIES
			// ARE BEING USED WITH OTHER THREADS?
			long delta = System.currentTimeMillis() - lastLoopTime;
			lastLoopTime = System.currentTimeMillis();

			// Redraw the graphics.
			view.renderGraphics();					
			
			// finally pause for a bit. Note: this should run us at about
			// 100 fps but on windows this might vary each loop due to
			// a bad implementation of timer
			try { Thread.sleep(10); } catch (Exception e) {}
		}while(gameSecondsLeft > 0);

		// Stop the timers.
		stopTimers();
		
		// Set the gamestate to 2.
		gameState = 2;
		view.setDrawingState(gameState);
		view.renderGraphics();		
	}

	// Getters for game information.
	public int getGameSecsLeft() {
		return gameSecondsLeft;
	}
	
	public void startTimers() {
		// Start the threads for the entites.
		gameTimer.start();
		playerShotsTimer.start();
		alienSpawnTimer.start();
		aliensMoveTimer.start();
	}
	
	public void stopTimers() {
		// Start the threads for the entites.
		gameTimer.stop();
		playerShotsTimer.stop();
		alienSpawnTimer.stop();
		aliensMoveTimer.stop();
	}
	
	

	// Inner listener classes.
	private class GameEntityDelayListener implements ActionListener {
		// constants measure in ms.
		public static final int GAME_TIME_DELAY = 1000;
		public static final int PLAYER_SHOTS_DELAY = 15;
		public static final int ALIEN_SPAWN_DELAY = 3000;
		public static final int ALIENS_MOVE_DELAY = 50;
		
		@Override
		public void actionPerformed(ActionEvent arg0) { 
			if(gameSecondsLeft > 0)				
			if(arg0.getSource() == gameTimer){
				gameSecondsLeft--;	
				view.setSecsLeftLabel(gameSecondsLeft);
			}else if(arg0.getSource() == playerShotsTimer){
					for(int i=0; i<shots.size();++i)
					if(shots.get(i).getLocation().y > -50){
						shots.get(i).setMoveUp(player.getFireStep());
						shots.get(i).moveUp();
					}
			}else if(arg0.getSource() == alienSpawnTimer){
				switch(aType) {
				case 0:
					aliens.add(
						new Sprite(res.getAlienImg1(), new Point((int)(Math.random()*(Screen.SCREEN_WIDTH - res.getAlienImg1().getIconWidth())),-50)));
					aType++;
					break;
				case 1:
					aliens.add(
							new Sprite(res.getAlienImg2(), new Point((int)(Math.random()*(Screen.SCREEN_WIDTH - res.getAlienImg2().getIconWidth())),-50)));
					aType++;
					break;
				case 2:
					aliens.add(
							new Sprite(res.getAlienImg3(), new Point((int)(Math.random()*(Screen.SCREEN_WIDTH - res.getAlienImg3().getIconWidth())),-50)));
					aType = 0;
					break;
				}
			}else if(arg0.getSource() == aliensMoveTimer){
				for(int i=0; i<aliens.size();++i)
					if(aliens.get(i).getLocation().y < Screen.SCREEN_HEIGHT+50){
						aliens.get(i).setMoveDown(5);
						aliens.get(i).moveDown();
						
						// Check for a collision. 
						// If the alien has collided with another shot sprite remove it and add a 
						// explosion sprite there.
						int j = 0;
						for(; j < shots.size(); ++j)
						    if(aliens.get(i).collided(shots.get(j))){
						    	aliens.remove(i);
						    	shots.remove(j);
						    	player.setScore(player.getScore()+1);
						    	view.setScoreLabel(player.getScore());
						    	// DISLIKE: This is annoying :/ sounds just like the shooting sound.
						    	// listener.splayer.playEnemyExpSound();
						    	j = shots.size();
						    }
					}
			}
		}	
	}

	private class GameClickListener implements MouseListener {

		@Override
		public void mousePressed(MouseEvent arg0) {
			if(gameState == 0){
				gameState = 1;
				view.setDrawingState(gameState);
				view.removeMouseListener(this);
				splayer.playGameStartSound();
				startTimers();
			}			
		}		
		// Unused methods.
		@Override
		public void mouseClicked(MouseEvent arg0) {}
		@Override
		public void mouseEntered(MouseEvent arg0) {}
		@Override
		public void mouseExited(MouseEvent arg0) {}
		@Override
		public void mouseReleased(MouseEvent arg0) {}
		
	}
	
	private class KeyBoardListener implements KeyListener {

		// The methods below is how the Game will listen to the Screen class.
		@Override
		public void keyPressed(KeyEvent e) {
			if(gameState == 1)
			switch(e.getKeyCode())
			{
				// Move player to the left.
			case KeyEvent.VK_LEFT:
				// The loop gives the animation a smoother, glider feel.
				for(int i = 0; i < 50;++i){
					if(player.getLocation().x > 0){
						player.moveLeft();
					}
				}
				break;
				// Move player to the right.
			case KeyEvent.VK_RIGHT:
				// The loop gives the animation a smoother, glider feel.
				for(int i = 0; i < 50;++i){
					if(player.getLocation().x < Screen.SCREEN_WIDTH - player.getIconWidth()){
						player.moveRight();
					}
				}
				break;
				// Shoot the fireball.
			case KeyEvent.VK_UP:
				splayer.playShootingSound();
				shots.add(player.shoot());
				break;
			}
		}

		@Override
		public void keyReleased(KeyEvent arg0) {}
		@Override
		public void keyTyped(KeyEvent arg0) {}
		
	}
		
}
