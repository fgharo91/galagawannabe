package Main;

import java.awt.Canvas;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import Models.ShooterSprite;
import Models.Sprite;
import Views.Screen;
import Views.SoundPlayer;
import Controls.GameEngine;
import Resources.Resources;

public class Main {
	public static void main(String[] args) {
		// Create an instance of main to get the resources for us.
		Resources res = new Resources();
		
		// Compose models of the game.
		ShooterSprite player = new ShooterSprite
		(res.getPlayerImg(), 
		 res.getFireBallImg(), 
		 new Point(Screen.SCREEN_WIDTH / 2, Screen.SCREEN_HEIGHT - res.getPlayerImg().getIconHeight()),
		 1, 1, 0, 0);		
		ArrayList<Sprite> shots = new ArrayList<Sprite>();
		ArrayList<Sprite> aliens = new ArrayList<Sprite>();
		
		// Compose the graphical interface variables and link them to the controller game.
		JFrame window = new JFrame("Galaga-Wannabe!");
		window.setResizable(false);
		Screen screen = new Screen(player, shots, aliens, GameEngine.gameSecondsLeft, player.getScore());
		window.add(screen);
		SoundPlayer splayer = new SoundPlayer(res);
		
		
		// Show the graphical interface.
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.pack();
		window.setVisible(true);
		
		// Compose main game control.
		GameEngine game = new GameEngine(player, shots, aliens, screen, splayer, res);

		// Start the main game loop.
		game.play();
	}

}
